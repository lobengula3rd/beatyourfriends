﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.Threading.Tasks;
using ApplicationServer.Models;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using ApplicationServer.Entities;


namespace ApplicationServer.Helpers
{
    [Authorize]
    public class GameController : ApiController
    {

        string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");

        // GET api/game/Basketball
        public IEnumerable<GameEntity> Get(string branchName)
        {
            CloudTable gamesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "games");
            // Construct the query operation for all customer entities where PartitionKey="Smith".
            TableQuery<GameEntity> query = new TableQuery<GameEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, branchName));
            return gamesTable.ExecuteQuery(query);
        }

        // GET api/game/2015-04-13
        public IEnumerable<GameEntity> Get(string branchName, DateTime fromDate, DateTime toDate)
        {
            CloudTable gamesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "games");
            // Construct the query operation for all customer entities where PartitionKey="Smith".
            string date1 = TableQuery.GenerateFilterConditionForDate("Scheduled", QueryComparisons.GreaterThanOrEqual, fromDate);
            string date2 = TableQuery.GenerateFilterConditionForDate("Scheduled", QueryComparisons.LessThanOrEqual, toDate);

            string finalFilter = TableQuery.CombineFilters(TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, branchName), TableOperators.And, date1), TableOperators.And, date2);

            TableQuery<GameEntity> rangeQuery = new TableQuery<GameEntity>().Where(finalFilter);
            return gamesTable.ExecuteQuery(rangeQuery);
        }

        // PUT api/game/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/game/5
        public void Delete(string branchName)
        {
        }

    }

}
