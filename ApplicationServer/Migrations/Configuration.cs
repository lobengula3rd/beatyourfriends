namespace ApplicationServer.Migrations
{
    using ApplicationServer.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationServer.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationServer.Models.ApplicationDbContext context)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            if (context.Users.FirstOrDefault(u => u.UserName == "admin") == null)
            {
                var user = new ApplicationUser() { UserName = "admin", Email = "admin@gmail.com" };
                user.Credit = 1000;
                IdentityResult result = UserManager.Create(user, "123456");
            }

            //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            //// Create Admin Role
            //string adminRole = "Admin";
            //IdentityResult roleResult;

            //// Check to see if Role Exists, if not create it
            //if (!RoleManager.RoleExists(adminRole))
            //{
            //    roleResult = RoleManager.Create(new IdentityRole(adminRole));
            //} 

        }
    }
}
