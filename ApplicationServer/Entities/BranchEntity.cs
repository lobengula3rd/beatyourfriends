﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace ApplicationServer.Entities
{
    public class BranchEntity : TableEntity
    {

        public BranchEntity(string name, string apiUrl, string imageUrl, bool tiePossible, string apiResult)
        {
            this.PartitionKey = "A-Z";
            this.RowKey = name;
            this.ApiUrl = apiUrl;
            this.ApiResult = apiResult;
            this.ImageUrl = imageUrl;
            this.TiePossible = tiePossible;
        }

        public BranchEntity() { }


        public string ApiUrl { get; set; }
        public string ApiResult { get; set; }
        public bool TiePossible { get; set; }
        public string ImageUrl { get; set; }

    }
}