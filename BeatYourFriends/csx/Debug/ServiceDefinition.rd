﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="BeatYourFriends" generation="1" functional="0" release="0" Id="6ee91b82-69fc-4428-96ee-e6b2d094867d" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="BeatYourFriendsGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="ApplicationServer:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/BeatYourFriends/BeatYourFriendsGroup/LB:ApplicationServer:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="ApplicationServer:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapApplicationServer:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="ApplicationServer:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapApplicationServer:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="ApplicationServerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapApplicationServerInstances" />
          </maps>
        </aCS>
        <aCS name="AppWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapAppWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="AppWorker:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapAppWorker:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="AppWorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapAppWorkerInstances" />
          </maps>
        </aCS>
        <aCS name="UiWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapUiWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="UiWorker:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapUiWorker:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="UiWorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/BeatYourFriends/BeatYourFriendsGroup/MapUiWorkerInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:ApplicationServer:Endpoint1">
          <toPorts>
            <inPortMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServer/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapApplicationServer:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServer/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapApplicationServer:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServer/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapApplicationServerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServerInstances" />
          </setting>
        </map>
        <map name="MapAppWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BeatYourFriends/BeatYourFriendsGroup/AppWorker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapAppWorker:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BeatYourFriends/BeatYourFriendsGroup/AppWorker/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapAppWorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/BeatYourFriends/BeatYourFriendsGroup/AppWorkerInstances" />
          </setting>
        </map>
        <map name="MapUiWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BeatYourFriends/BeatYourFriendsGroup/UiWorker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapUiWorker:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BeatYourFriends/BeatYourFriendsGroup/UiWorker/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapUiWorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/BeatYourFriends/BeatYourFriendsGroup/UiWorkerInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="ApplicationServer" generation="1" functional="0" release="0" software="C:\Users\itaigendler\workspace\beatyourfriends\BeatYourFriends\csx\Debug\roles\ApplicationServer" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;ApplicationServer&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ApplicationServer&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;AppWorker&quot; /&gt;&lt;r name=&quot;UiWorker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="AppWorker" generation="1" functional="0" release="0" software="C:\Users\itaigendler\workspace\beatyourfriends\BeatYourFriends\csx\Debug\roles\AppWorker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;AppWorker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ApplicationServer&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;AppWorker&quot; /&gt;&lt;r name=&quot;UiWorker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/BeatYourFriends/BeatYourFriendsGroup/AppWorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/BeatYourFriends/BeatYourFriendsGroup/AppWorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/BeatYourFriends/BeatYourFriendsGroup/AppWorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="UiWorker" generation="1" functional="0" release="0" software="C:\Users\itaigendler\workspace\beatyourfriends\BeatYourFriends\csx\Debug\roles\UiWorker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;UiWorker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ApplicationServer&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;AppWorker&quot; /&gt;&lt;r name=&quot;UiWorker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/BeatYourFriends/BeatYourFriendsGroup/UiWorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/BeatYourFriends/BeatYourFriendsGroup/UiWorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/BeatYourFriends/BeatYourFriendsGroup/UiWorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="ApplicationServerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="UiWorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="AppWorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="ApplicationServerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="AppWorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="UiWorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="ApplicationServerInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="AppWorkerInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="UiWorkerInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="da0e719b-f5bf-42e8-8592-ce2cbfbeb679" ref="Microsoft.RedDog.Contract\ServiceContract\BeatYourFriendsContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="ccabe015-f7a0-4e46-b236-1ccb0d8f3009" ref="Microsoft.RedDog.Contract\Interface\ApplicationServer:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/BeatYourFriends/BeatYourFriendsGroup/ApplicationServer:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>