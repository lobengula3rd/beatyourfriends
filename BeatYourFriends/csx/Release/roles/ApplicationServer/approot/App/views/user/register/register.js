﻿'use strict';

angular.module('beatYourFriends.user.registerView', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("user.register", {
        url: '/register',
        templateUrl: '/app/views/user/register/register.html',
        controller: 'registerController',
        data: {
            rule: function () {
                if (localStorage.accessToken)
                    return { newState: "user" };
            },
            title: "Register"
        }
    });

}])

.controller('registerController', ['$scope', '$state', 'User', 'Dialog', function ($scope, $state, User, Dialog) {

    // $scope.user = { username: "", password: "", confirmPassword: "", email: "", firstName: "", lastName: "", dateOfBirth: "" };
    $scope.user = { username: "", password: "", confirmPassword: "", email: "" };

    $scope.inProcess = false;


    $scope.register = function (form) {
        //validate input
        $scope.inProcess = true;
        User.register($scope.user, function (err, res) {
            $scope.inProcess = false;
            if (err)
                Dialog.open("Error", err.data.Message, "md");
            else
                Dialog.open("Success", "Registration completed Successfully. Please log in.", "md", function () {
                    $state.go("user.login");
                });
        });
    }



}]);