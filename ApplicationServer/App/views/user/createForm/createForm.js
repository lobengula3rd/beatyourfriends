﻿'use strict';

angular.module('beatYourFriends.user.createFormView', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("user.createForm", {
        url: '/createForm',
        templateUrl: '/app/views/user/createForm/createForm.html',
        controller: 'createFormController',
        data: {
            rule: function () {
                if (!localStorage.accessToken)
                    return { newState: "user" };
            },
            title: "User - Create a new form"
        }
    });

}])

.controller('createFormController', ['$scope', '$state', 'User', 'Game', 'Branch', 'Dialog', 'Form', function ($scope, $state, User, Game, Branch, Dialog, Form) {

    $scope.games = [];
    $scope.form = [];
    $scope.amount = 0;
    $scope.branches;

    $scope.inProcess = false;
    $scope.initialLoading = true;



    var today = new Date();
    var sevenDaysFromNow = new Date();
    sevenDaysFromNow.setDate(sevenDaysFromNow.getDate() + 7);

    Branch.getBranches(function (err, res) {
        if (res) {
            var branches = res.data;
            $scope.branches = branches;
            for (var i = 0; i < branches.length; i++) {
                Game.getAllGamesBetweenDates(branches[i].RowKey, today, sevenDaysFromNow, function (err, res) {
                    $scope.initialLoading = false;
                    if (res)
                        for (var i = 0; i < res.data.length; i++) {
                            for (var j = 0; j < branches.length; j++) {
                                if (branches[j].RowKey === res.data[i].PartitionKey)
                                    res.data[i].ImageUrl = branches[j].ImageUrl;
                                if (branches[j].RowKey === res.data[i].PartitionKey)
                                    res.data[i].TiePossible = branches[j].TiePossible;
                            }
                            res.data[i].Scheduled = new Date(res.data[i].Scheduled);
                            $scope.games.push(res.data[i]);
                        }
                });

            }
        }
        $scope.initialLoading = false;

    });

    $scope.addGameToForm = function (game) {
        if ($scope.form.length > 15)
            return Dialog.open("Error", "Maximum bets per form is 16", "md");
        game.choice = -1;
        $scope.form.push(game);
        $scope.games.splice($scope.games.indexOf(game), 1);
    };

    $scope.removeGameFromForm = function (game) {
        $scope.games.push(game);
        $scope.form.splice($scope.form.indexOf(game), 1);
    };


    $scope.calculateBetClass = function (bet, team) {
        if (bet.choice === 0 && team === 0)
            return 'bet-choice pointer fill';
        if (bet.choice === 1 && team === 1)
            return 'bet-choice pointer fill';
        if (bet.choice === 2 && team === 2)
            return 'bet-choice pointer fill';
        if (team === 1 && !bet.TiePossible)
            return 'pointer low';
        return 'pointer fill';
    };

    $scope.sendForm = function () {
        $scope.inProcess = true;
        for (var i = 0; i < $scope.form.length; i++) {
            if ($scope.form[i].choice === -1) {
                $scope.inProcess = false;
                return Dialog.open("Error", "you must place your bet on each game", "md");
            }
            else
                $scope.form[i].yourChoice = $scope.form[i].choice;
        }
        if ($scope.amount <= 0 || $scope.amount > 5000) {
            $scope.inProcess = false;
            return Dialog.open("Error", "your amount of bet must be between 1-5000");
        }
        Form.createForm($scope.form, $scope.amount, function (err, res) {
            $scope.inProcess = false;
            if (err)
                Dialog.open("Error", err.data.Message, "md");
            else
                $state.go('user.panel');
        })

    };

    $scope.moveToPickForm = function () {
        $state.go('user.pickForm');
    }


}]);