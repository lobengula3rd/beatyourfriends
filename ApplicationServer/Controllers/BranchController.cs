﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.Threading.Tasks;
using ApplicationServer.Models;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using ApplicationServer.Entities;


namespace ApplicationServer.Helpers
{
    [Authorize]
    public class BranchController : ApiController
    {

        string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");

        // GET api/branch
        public IEnumerable<BranchEntity> Get()
        {
            CloudTable branchesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "branches");
            TableQuery<BranchEntity> query = new TableQuery<BranchEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "A-Z"));
            return branchesTable.ExecuteQuery(query);
        }

        [Authorize(Users = "admin")]
        // POST api/branch
        public async Task<HttpResponseMessage> Post()
        {

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return Request.CreateErrorResponse(HttpStatusCode.UnsupportedMediaType, "method is not allowed");
            }


            var provider = new MultipartFormDataStreamProvider(Path.GetTempPath());
            try
            {
                // read the form data
                await Request.Content.ReadAsMultipartAsync(provider);

            }
            catch (Exception ex)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            CloudBlobContainer container = AzureStorageHelpers.GetWebApiContainer(connectionString, "branches");


            var branchName = provider.FormData["name"];
            var x = provider.FormData["tiePossible"];
            bool tiePossible = Convert.ToBoolean(provider.FormData["tiePossible"]);
            var apiUrl = provider.FormData["apiUrl"];
            var apiResult = provider.FormData["apiResult"];


            MultipartFileData fileData;
            //string fileName;
            Stream fileStream;
            try
            {
                fileData = provider.FileData.FirstOrDefault(file => file.Headers.ContentDisposition.Name.Contains("ContentApk"));
                //fileName = MultipartHelper.GetFileName(fileData);
                fileStream = MultipartHelper.GetStream(fileData);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "can't process file");
            }

            // Retrieve reference to a blob
            CloudBlockBlob blob = container.GetBlockBlobReference(branchName);
            blob.Properties.ContentType = fileData.Headers.ContentType.MediaType;
            blob.UploadFromStream(fileStream);
            MultipartHelper.CleanFileData(fileData, fileStream);
            FileDetails createdFile = (new FileDetails
            {
                ContentType = blob.Properties.ContentType,
                Name = blob.Name,
                Size = blob.Properties.Length,
                Location = blob.Uri.AbsoluteUri
            });


            BranchEntity newBranch = new BranchEntity(branchName, apiUrl, createdFile.Location, tiePossible, apiResult);
            CloudTable branchesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "branches");
            TableOperation insertOperation = TableOperation.Insert(newBranch);

            try
            {
                TableResult result = await branchesTable.ExecuteAsync(insertOperation);
                // pass message to worker 1 to handle the new sport branch (bring its games)
                CloudQueue newBranchQueue = AzureStorageHelpers.GetWebApiQueue(connectionString, "newbranch");
                CloudQueueMessage message = new CloudQueueMessage(branchName + "\n" + apiUrl + "\n" + apiResult);
                newBranchQueue.AddMessage(message);
                return Request.CreateErrorResponse(HttpStatusCode.OK, "branch created");
            }
            catch (Exception e)
            {
                if (e.Message.Contains("409"))
                    return Request.CreateErrorResponse(HttpStatusCode.Conflict, "branch name is alredy taken");
                else
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");
            }
        }

        // PUT api/branches/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/branches/5
        [Authorize(Users = "admin")]
        public void Delete(string branchName)
        {
            CloudTable branchesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "branches");
            TableOperation retrieveOperation = TableOperation.Retrieve<BranchEntity>("A-Z", branchName);
            TableResult retrievedResult = branchesTable.Execute(retrieveOperation);
            BranchEntity deleteEntity = (BranchEntity)retrievedResult.Result;
            if (deleteEntity != null)
            {
                TableOperation deleteOperation = TableOperation.Delete(deleteEntity);
                // Execute the operation.
                branchesTable.Execute(deleteOperation);
            }
        }

    }

    public class FileDetails
    {
        public string Name { get; set; }
        public long Size { get; set; }
        public string ContentType { get; set; }
        public string Location { get; set; }
    }
}
