﻿using System;
using System.Collections.Generic;

namespace ApplicationServer.Models
{
    // Models returned by AccountController actions.

    public class newBranchModel
    {
        public string Name { get; set; }

        public string ApiUrl { get; set; }
    }

}
