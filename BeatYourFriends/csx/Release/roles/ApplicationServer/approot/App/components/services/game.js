﻿'use strict';

angular.module('beatYourFriends.services')

.service('Game', ['$http', '$upload', function ($http, $upload) {

    this.getAllBranchGames = function (branchName, callback) {
        $http({
            method: 'GET',
            url: '/api/Game',
            responseType: 'json',
            params: { branchName: branchName }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getAllGamesBetweenDates = function (branchName, fromDate, toDate, callback) {
        $http({
            method: 'GET',
            url: '/api/Game',
            responseType: 'json',
            params: { branchName: branchName, fromDate: fromDate, toDate: toDate }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };





}]);