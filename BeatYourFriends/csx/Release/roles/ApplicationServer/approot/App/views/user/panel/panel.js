﻿'use strict';

angular.module('beatYourFriends.user.panelView', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("user.panel", {
        url: '/panel',
        templateUrl: '/app/views/user/panel/panel.html',
        controller: 'panelController',
        data: {
            rule: function () {
                if (!localStorage.accessToken)
                    return { newState: "user" };
            },
            title: "User - Panel"
        }
    });

}])

.controller('panelController', ['$scope', '$rootScope', '$state', 'Dialog', 'Form', 'Bet', function ($scope, $rootScope, $state, Dialog, Form, Bet) {

    $scope.bets = [];
    $scope.chosenBet;

    $scope.initialLoading = true;

    $scope.userName = localStorage.userName;


    $rootScope.$emit('calcCredit');

    var getBets = function () {
        Bet.getBets(function (err, res) {
            if (res && !res.data.length)
                $scope.initialLoading = false;
            else if (res) {
                for (var i = 0; i < res.data.length; i++) {
                    var x = JSON.parse(res.data[i].Games);
                    res.data[i].Games = JSON.parse(res.data[i].Games);
                    getFormOfBet(res.data[i]);
                }
            }
        })
    };

    var getFormOfBet = function (bet) {
        Form.getForm(bet.RowKey, function (err, res) {
            if (res) {
                bet.AmountOfUsersBet = res.data[0].AmountOfUsersBet;
                bet.FinishDate = res.data[0].LastDateToBet;
                bet.Winner = res.data[0].Winner;
                $scope.initialLoading = false;
                $scope.bets.push(bet);
            }
        });
    }

    getBets();

    $scope.chooseBet = function (bet) {
        $scope.chosenBet = bet;
    }

    $scope.calculateBetClass = function (bet, team) {
        if (bet.yourChoice === 0 && team === 0)
            return 'bet-choice pointer fill';
        if (bet.yourChoice === 1 && team === 1)
            return 'bet-choice pointer fill';
        if (bet.yourChoice === 2 && team === 2)
            return 'bet-choice pointer fill';
        if (team === 1 && !bet.TiePossible)
            return 'pointer low';
        return 'pointer fill';
    };

    $scope.calculateWellClass = function (bet) {
        if (bet) {
            if (bet.Winner) {
                if (bet.Winner === localStorage.userName)
                    return 'well user-winner';
                else
                    return 'well user-lost';
            }
            else
                return 'well';
        }
    }

    $scope.moveToPickForm = function () {
        $state.go('user.pickForm');
    };

    $scope.moveToCreateForm = function () {
        $state.go('user.createForm');
    };



}]);