﻿'use strict';

angular.module('beatYourFriends.user.pickFormView', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("user.pickForm", {
        url: '/pickForm',
        templateUrl: '/app/views/user/pickForm/pickForm.html',
        controller: 'pickFormController',
        data: {
            rule: function () {
                if (!localStorage.accessToken)
                    return { newState: "user" };
            },
            title: "User - Pick a form to bet on"
        }
    });

}])

.controller('pickFormController', ['$scope', '$state', 'User', 'Game', 'Branch', 'Dialog', 'Form', 'Bet', function ($scope, $state, User, Game, Branch, Dialog, Form, Bet) {

    $scope.forms = [];
    $scope.userAlreadyBetedFormRowKeys = [];
    $scope.chosenForm;

    $scope.inProcess = false;
    $scope.initialLoading = true;


    var getForm = function () {
        Bet.getBets(function (err, res) {
            if (res) {
                for (var i = 0; i < res.data.length; i++)
                    $scope.userAlreadyBetedFormRowKeys.push(res.data[i].RowKey);
                for (var i = 0; i < 7; i++) {
                    var date = new Date(new Date().setDate(new Date().getDate() - i));
                    Form.getForms(date, function (err, res) {
                        if (err)
                            console.log(err)
                        else if (!res.data.length)
                            $scope.initialLoading = false;
                        else
                            for (var i = 0; i < res.data.length; i++) {
                                if ($scope.userAlreadyBetedFormRowKeys.indexOf(res.data[i].RowKey) === -1) {
                                    res.data[i].Games = JSON.parse(res.data[i].Games);
                                    for (var j = 0; j < res.data[i].Games.length; j++)
                                        res.data[i].Games[j].yourChoice = -1;
                                    $scope.initialLoading = false;
                                    $scope.forms.push(res.data[i]);
                                }
                            }
                    });
                }
            }
        });

    };

    getForm();

    $scope.calculateBetClass = function (bet, team) {
        if (bet.yourChoice === 0 && team === 0)
            return 'bet-choice pointer fill';
        if (bet.yourChoice === 1 && team === 1)
            return 'bet-choice pointer fill';
        if (bet.yourChoice === 2 && team === 2)
            return 'bet-choice pointer fill';
        if (team === 1 && !bet.TiePossible)
            return 'pointer low';
        return 'pointer fill';
    };

    $scope.sendForm = function () {
        $scope.inProcess = true;
        for (var i = 0; i < $scope.chosenForm.Games.length; i++) {
            if ($scope.chosenForm.Games[i].yourChoice === -1) {
                $scope.inProcess = false;
                return Dialog.open("Error", "you must place your bet on each game", "md");
            }
        }
        var amount = $scope.chosenForm.Amount;
        var games = JSON.stringify($scope.chosenForm.Games);
        var formRowKey = $scope.chosenForm.RowKey;
        var formCreatedBy = $scope.chosenForm.CreatedBy;
        Bet.betOnForm(amount, games, formRowKey, formCreatedBy, function (err, res) {
            $scope.inProcess = false;
            if (err)
                Dialog.open("Error", err.data.Message, "md");
            else
                $state.go('user.panel');
        });
    };

    $scope.chooseForm = function (form) {
        if ($scope.chosenForm) {
            $scope.forms.push($scope.chosenForm);
            $scope.chosenForm = null;
        }
        $scope.chosenForm = form;
        $scope.forms.splice($scope.forms.indexOf(form), 1);

    }

    $scope.moveToCreateForm = function () {
        $state.go('user.createForm');
    }




}]);