﻿'use strict';

angular.module('beatYourFriends.auth', [])

.factory('authHttpInterceptor', ['$q', '$injector', function ($q, $injector) {

    return {

        request: function (config) {
            config.headers = config.headers || {};
            if (localStorage.accessToken) {
                config.headers.Authorization = 'Bearer ' + localStorage.accessToken;
            }
            return config;
        },

        response: function (response) {
            if (response.status === 401) {
                console.log("Response 401");
            }
            return response || $q.when(response);
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                console.log("Response Error 401", rejection);

                localStorage.removeItem("accessToken");

                var $state = $injector.get('$state');
                $state.go("user.login");
            }
            return $q.reject(rejection);
        }
    }
}])


.service('Auth', ['$http', '$rootScope', function ($http, $rootScope) {

    this.logOut = function (callback) {
        delete localStorage.accessToken;
        delete localStorage.userName;
        $rootScope.$emit('loggedOut');
        callback();
    };

    this.internalLogin = function (user, callback) {
        $http({
            method: 'POST',
            url: '/Token',
            responseType: 'json',
            data: $.param(user)
        }).then
        (function (res) {
            localStorage.accessToken = res.data.access_token;
            localStorage.userName = res.data.userName;
            $rootScope.$emit('loggedIn');
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err.data.error_description);
        });
    };

}]);
