﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Web.Http.Tracing;
using System.Net.Http;
using ApplicationServer.TraceWriters;

namespace ApplicationServer.Hubs
{
    public class ChatHub : SignalRTracer
    {
        
        public void Send()
        {
            Clients.All.updateStatus();
        }

        public void Send(string txt)
        {
            WriteLog("Sending Message: " + txt);
            Clients.All.updateStatus(txt);
        }

        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }
    }
}