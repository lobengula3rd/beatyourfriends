﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.Threading.Tasks;
using ApplicationServer.Models;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using ApplicationServer.Entities;
using Newtonsoft.Json;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;


namespace ApplicationServer.Helpers
{
    [Authorize]
    public class BetController : ApiController
    {

        string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");


        // GET api/bet/:date
        public IEnumerable<BetEntity> Get()
        {
            CloudTable betsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "bets");
            TableQuery<BetEntity> query = new TableQuery<BetEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, User.Identity.Name));
            return betsTable.ExecuteQuery(query);
        }

        // POST api/bet
        public async Task<IHttpActionResult> Post(newBetModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindByName(User.Identity.Name);
            if (user.Credit < model.Amount)
                return BadRequest("You don't have enough credit to place this bet");

            CloudTable betTable = AzureStorageHelpers.GetWebApiTable(connectionString, "bets");
            BetEntity newBet = new BetEntity(model.Amount, model.Games, User.Identity.Name, model.FormRowKey, model.FormCreatedBy);
            TableOperation insertOperation = TableOperation.Insert(newBet);
            betTable.Execute(insertOperation);

            CloudTable formsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "forms");
            TableQuery<FormEntity> query = new TableQuery<FormEntity>().Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, model.FormRowKey));
            IEnumerable<FormEntity> list = formsTable.ExecuteQuery(query);
            FormEntity formToUpdate = list.First();

            formToUpdate.AmountOfUsersBet++;
            TableOperation updateOperation = TableOperation.Replace(formToUpdate);
            formsTable.Execute(updateOperation);


            user.Credit = user.Credit - model.Amount;
            manager.Update(user);

            return Ok();
        }

        // PUT api/bet/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/bet/5
        public void Delete(string branchName)
        {
        }

    }

}
