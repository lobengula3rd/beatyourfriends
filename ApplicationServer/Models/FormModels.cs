﻿using System;
using System.Collections.Generic;

namespace ApplicationServer.Models
{
    // Models returned by AccountController actions.

    public class newFormModel
    {
        public int Amount { get; set; }

        public string Games { get; set; }
    }

}
