﻿'use strict';

angular.module('beatYourFriends.user.loginView', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("user.login", {
        url: '/login',
        templateUrl: '/app/views/user/login/login.html',
        controller: 'loginController',
        data: {
            rule: function () {
                if (localStorage.accessToken)
                    return { newState: "user" };
            },
            title: "User Login"
        }
    });

}])

.controller('loginController', ['$scope', '$state', 'Dialog', 'Auth', function ($scope, $state, Dialog, Auth) {

    $scope.user = { username: "", password: "", rememberMe: true, grant_type: "password" };

    $scope.inProcess = false;

    $scope.internalLogin = function (form) {
        //validate input
        $scope.inProcess = true;
        Auth.internalLogin($scope.user, function (err, res) {
            $scope.inProcess = false;
            if (err) {
                Dialog.open('Error', err, 'md');
            }
            else {
                //Dialog.open('Success', "connected", 'md', function (res) {
                $state.go(localStorage.subApp);
                //});
            }
        });
    }



}]);