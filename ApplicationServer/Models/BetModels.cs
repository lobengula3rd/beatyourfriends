﻿using System;
using System.Collections.Generic;

namespace ApplicationServer.Models
{
    // Models returned by AccountController actions.

    public class newBetModel
    {
        public int Amount { get; set; }

        public string Games { get; set; }

        public string FormRowKey { get; set; }

        public string FormCreatedBy { get; set; }
    }

}
