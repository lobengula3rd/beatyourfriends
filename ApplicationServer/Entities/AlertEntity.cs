﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace ApplicationServer.Entities
{
    public class AlertEntity : TableEntity
    {

        public AlertEntity(string username, string message, string header)
        {
            this.PartitionKey = username;
            this.RowKey = (DateTime.MaxValue.Ticks - DateTime.UtcNow.Ticks).ToString();
            this.Message = message;
            this.Header = header;
        }

        public AlertEntity() { }


        public string Message { get; set; }
        public string Header { get; set; }
    }
}