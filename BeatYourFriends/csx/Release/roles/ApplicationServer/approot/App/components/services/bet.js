﻿'use strict';

angular.module('beatYourFriends.services')

.service('Bet', ['$http', function ($http) {

    this.betOnForm = function (amount, games, formRowKey, formCreatedBy, callback) {
        $http({
            method: 'POST',
            url: '/api/bet',
            responseType: 'json',
            data: { games: JSON.stringify(games), amount: amount, formRowKey: formRowKey, formCreatedBy: formCreatedBy }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getBets = function (callback) {
        $http({
            method: 'GET',
            url: '/api/bet',
            responseType: 'json'
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };




}]);