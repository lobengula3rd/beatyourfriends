﻿'use strict';

angular.module('beatYourFriends.dialog', [])

.service('Dialog', [function () {

    var self = this;

    this.open = function (title, content, size, successCallback, dismissCallback) {
        self.opened = true;
        self.content = content;
        self.title = title;
        self.successCallback = successCallback;
        self.dismissCallback = dismissCallback;
        if (size)
            self.size = size;
        if (self.successCallback && self.dismissCallback)
            self.isQuestion = true;
    };

    this.closed = function (err, success) {
        self.opened = false;
        self.size = "";
        self.content = "";
        self.title = "";
        self.isQuestion = false;
        if (err && self.dismissCallback)
            self.dismissCallback();
        else if (self.successCallback)
            self.successCallback();
    };

    this.opened = false;

    this.size = "";

    this.content = "";

    this.title = "";

    this.isQuestion = false;

    this.successCallback = null;

    this.dismissCallback = null;

}])

.controller('ModalDemoCtrl', ['$scope', '$state', '$modal', 'Dialog', function ($scope, $state, $modal, Dialog) {

    $scope.dialog = Dialog;

    $scope.$watch('dialog.opened', function (newValue, oldValue) {
        if (newValue !== oldValue && newValue) {
            $scope.content = Dialog.content;
            $scope.title = Dialog.title;
            $scope.isQuestion = Dialog.isQuestion;
            if (Dialog.size)
                $scope.open(Dialog.size);
            else
                $scope.open();
        }
    });


    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                content: function () { return $scope.content },
                title: function () { return $scope.title },
                isQuestion: function () { return $scope.isQuestion }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            Dialog.closed(null, "ok");
        }, function () {
            Dialog.closed("err");
        });
    };

}])

.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'content', 'title', 'isQuestion', function ($scope, $modalInstance, content, title, isQuestion) {

    $scope.content = content;
    $scope.title = title;
    $scope.isQuestion = isQuestion;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss();
    };

    //$scope.cancel = function () {
    //    $modalInstance.dismiss('cancel');
    //};

}]);