﻿'use strict';

angular.module('beatYourFriends.services')

.service('Branch', ['$http', '$upload', function ($http, $upload) {

    this.getBranches = function (callback) {
        $http({
            method: 'GET',
            url: '/api/Branch',
            responseType: 'json'
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.createBranch = function (newBranch, progressFunction, callback) {
        var uploadData = { name: newBranch.name, tiePossible: newBranch.tiePossible, apiUrl: newBranch.apiUrl, apiResult: newBranch.apiResult };

        var upload = $upload.upload({
            method: 'POST',
            url: '/api/Branch',
            data: uploadData,
            file: newBranch.imageFile,
            fileFormDataName: "ContentApk"
        });

        upload.progress(progressFunction);

        upload.then(function (res) {
            if (callback)
                callback(null, res.data);
        },
        function (err) {
            if (callback)
                callback(err);
        });

        return upload;
    };

    this.deleteBranch = function (branch, callback) {
        $http({
            method: 'DELETE',
            url: '/api/Branch',
            responseType: 'json',
            params: { branchName: branch.RowKey }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    }



}]);