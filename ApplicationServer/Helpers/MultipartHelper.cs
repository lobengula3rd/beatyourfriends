﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;

namespace ApplicationServer.Helpers
{
    public static class MultipartHelper
    {
        public static String GetFileName(MultipartFileData file)
        {
            if (file == null)
            {
                return null;
            }
            string fileName = Regex.Replace(file.Headers.ContentDisposition.FileName, "\"", "");
            return Path.GetFileName(fileName);
        }

        public static Stream GetStream(MultipartFileData file)
        {
            if (file == null)
            {
                return null;
            }
            string path = file.LocalFileName;
            Stream inputStream = File.OpenRead(path);
            return inputStream;
        }

        public static void CleanFileData(MultipartFileData file, Stream stream)
        {
            try
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
            catch (Exception e)
            {
            }

            try
            {
                if (file != null)
                {
                    File.Delete(file.LocalFileName);
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}
