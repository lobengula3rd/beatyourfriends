﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace ApplicationServer.Entities
{
    public class BetEntity : TableEntity
    {

        public BetEntity(int amount, string games, string username, string formRowKey, string formCreatedBy)
        {
            this.PartitionKey = username;
            this.RowKey = formRowKey;
            this.Amount = amount;
            this.Games = games;
            this.FormCreatedBy = formCreatedBy;
        }

        public BetEntity() { }


        public int Amount { get; set; }
        public string Games { get; set; }
        public string FormCreatedBy { get; set; }
    }
}