﻿'use strict';

angular.module('beatYourFriends.user', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("user", {
        url: '/user',
        template: '<ui-view></ui-view>', 
        data: {
            rule: function () {
                if (localStorage.accessToken)
                    return { newState: "user.createForm" };
                else
                    return { newState: "user.login" };
            }
        }
    });

}])
