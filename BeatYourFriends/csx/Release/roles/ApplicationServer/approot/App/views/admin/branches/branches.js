﻿'use strict';

angular.module('beatYourFriends.admin.branchesView', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("admin.branches", {
        url: '/branches',
        templateUrl: '/app/views/admin/branches/branches.html',
        controller: 'adminBranchesController',
        data: {
            rule: function () {
                if (!(localStorage.accessToken && localStorage.userName === "admin"))
                    return { newState: "user" };
            },
            title: "Admin - Branches"
        }
    });

}])

.controller('adminBranchesController', ['$scope', '$state', 'Dialog', 'User', 'Branch', 'Game', function ($scope, $state, Dialog, User, Branch, Game) {

    $scope.newBranch = { name: "", apiUrl: "", tiePossible: false, imageFile: null, apiResult: "" };
    $scope.error = "";
    $scope.branches = {};

    $scope.inProcess = true;

    var getBranches = function () {
        Branch.getBranches(function (err, res) {
            $scope.inProcess = false
            if (err)
                Dialog.opne("Error", err, "md");
            else {
                $scope.branches = res.data;
                recursiveCalculateAmount(0, $scope.branches);
            }
        });
    };

    var recursiveCalculateAmount = function (i, branches) {
        if (i < branches.length)
            getAllBranchGames(branches[i].RowKey, function (err, res) {
                if (res)
                    branches[i].GamesAmount = res.data.length;
                recursiveCalculateAmount(++i, branches);
            });
        else
            return;
    };

    var getAllBranchGames = function (branchName, callback) {
        Game.getAllBranchGames(branchName, function (err, res) {
            if (err)
                callback(err);
            else
                callback(null, res);
        });
    };

    var resetForm = function () {
        $scope.newBranch = { name: "", apiUrl: "", tiePossible: false, imageFile: null, apiResult: "" };
        $scope.error = "";
        $("#newBranchForm").trigger('reset');
    };

    getBranches();



    $scope.createBranch = function (form) {
        $scope.inProcess = true;
        Branch.createBranch($scope.newBranch, updateWhileUploading, function (err, res) {
            $scope.inProcess = false;
            if (err) {
                if (err.status !== 401)
                    Dialog.open("Error", err.data.Message, "md");
            }
            else {
                resetForm();
                getBranches();
            }
        });
    };

    $scope.deleteBranch = function (branch) {
        $scope.inProcess = true;
        Branch.deleteBranch(branch, function (err, res) {
            $scope.inProcess = false;
            if (err)
                Dialog.open("Error", err.data.Message, "md");
            else
                getBranches();
        });
    };

    var updateWhileUploading = function (evt) {
        var percent = parseInt(100.0 * evt.loaded / evt.total);
        console.log(percent);
    };

    var uploadCallback = function (err, res) {
        if (err) {
            removeProgressBar();
            if (err.data.Message)
                Dialog.open(Language.strings.mobileAnalyzer.scanError, err.data.Message, dialogTypesEnum.message, function () {
                    $scope.resetScanForm();
                });
            else if (!err.config.file.size)
                Dialog.open(Language.strings.mobileAnalyzer.scanError, Language.strings.mobileAnalyzer.scanMissingFile, dialogTypesEnum.message, function () {
                    $scope.resetScanForm();
                });
            else if (scanAborted)
                Dialog.open(Language.strings.mobileAnalyzer.scanError, Language.strings.mobileAnalyzer.scanAborted, dialogTypesEnum.message, function () {
                    $scope.resetScanForm();
                });
            else
                Dialog.open(Language.strings.mobileAnalyzer.scanError, Language.strings.mobileAnalyzer.scanErrorOccured, dialogTypesEnum.message, function () {
                    $scope.resetScanForm();
                });
        }
        else {

        }
    };

    $scope.abortUpload = function () {
        scanAborted = true;
        $scope.uploader.abort();
    }

    $scope.fileNameChanged = function (newFile) {
        if (newFile.files)
            $scope.newBranch.imageFile = newFile.files[0];
        else
            $scope.newBranch.imageFile = newFile[0];

        var fileSize = $scope.newBranch.imageFile.size;
        var fileName = $scope.newBranch.imageFile.name;
        var lastIndex = fileName.lastIndexOf(".");
        var extension = fileName.substring(lastIndex).toLowerCase();
        if (!isImageFileExtension(extension)) {
            $scope.error = "selected file is not an image";
            $scope.newBranch.imageFile = null;
        }
        else if (fileSize > 1024 * 1000 * 5) {
            $scope.error = "image can not exceed 5MB"
            $scope.newBranch.imageFile = null;
        }
        else
            $scope.error = "success";

        // called outside of angular.js scope
        $scope.$apply();
    };


    var isImageFileExtension = function (ext) {
        if (ext === ".jpeg" || ext === ".jpg" || ext === "gif" || ext === "png" || ext === "png" || ext === "jfif" || ext === "tiff" || ext === "bmp")
            return true;
        else
            return false;
    };


}]);