﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace ApplicationServer.Entities
{
    public class FormEntity : TableEntity
    {

        public FormEntity(string partitionKey, string rowKey, int amount, string games, DateTime checkWinnerDate, string createdBy, DateTime lastDateToBet)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
            this.Amount = amount;
            this.Games = games;
            this.CheckWinnerDate = checkWinnerDate;
            this.LastDateToBet = lastDateToBet;
            this.CreatedBy = createdBy;
            this.Winner = "";
            this.AmountOfUsersBet = 1;
        }

        public FormEntity() { }


        public int Amount { get; set; }
        public string Games { get; set; }
        public DateTime CheckWinnerDate { get; set; }
        public DateTime LastDateToBet { get; set; }
        public string Winner { get; set; }
        public string CreatedBy { get; set; }
        public int AmountOfUsersBet { get; set; }
    }
}