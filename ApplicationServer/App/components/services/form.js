﻿'use strict';

angular.module('beatYourFriends.services')

.service('Form', ['$http', function ($http) {

    this.createForm = function (games, amount, callback) {
        $http({
            method: 'POST',
            url: '/api/form',
            responseType: 'json',
            data: { games: JSON.stringify(games), amount: amount }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getForms = function (creationDate, callback) {
        $http({
            method: 'GET',
            url: '/api/form',
            responseType: 'json',
            params: { creationDate: creationDate }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getForm = function (formRowKey, callback) {
        $http({
            method: 'GET',
            url: '/api/form',
            responseType: 'json',
            params: { formRowKey: formRowKey }
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };




}]);