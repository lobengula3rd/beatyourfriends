﻿'use strict';

angular.module('beatYourFriends', [
  //3rd party modules
  'ui.router',
  'ngAnimate',
  'ui.bootstrap',
  'angularFileUpload',

  'beatYourFriends.navbar',


  //User Views
  'beatYourFriends.user',
  'beatYourFriends.user.loginView',
  'beatYourFriends.user.registerView',
  'beatYourFriends.user.createFormView',
  'beatYourFriends.user.pickFormView',
  'beatYourFriends.user.panelView',

  //Admin Views
  'beatYourFriends.admin',
  'beatYourFriends.admin.branchesView',

  //Components
  'beatYourFriends.services',
  'beatYourFriends.auth',
  'beatYourFriends.dialog',
  'beatYourFriends.title'

])


.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', '$provide', function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $provide) {

    $urlRouterProvider.otherwise(function ($injector, helpers) {

        var $state = $injector.get('$state');
        if (localStorage.accessToken && localStorage.userName === 'admin')
            return $state.go('admin');
        $state.go('user');
    });

    $httpProvider.interceptors.push('authHttpInterceptor');

    $locationProvider.hashPrefix('!').html5Mode({
        enabled: true,
        requireBase: false
    });

}])

.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {

    $rootScope.$on('$stateChangeStart', function (evt, toState) {
        // check the states rule
        if (angular.isFunction(toState.data.rule)) {
            var result = toState.data.rule();
            if (result && result.newState) {
                evt.preventDefault();
                $state.go(result.newState);
            }
        }
    });

    $rootScope.$on('$stateChangeSuccess', function (evt, toState) {
        if (toState.name.indexOf("user") !== -1)
            localStorage.subApp = "user";
        else if (toState.name.indexOf("admin") !== -1)
            localStorage.subApp = "admin";
    });

    var x = { "id": "0371fa25-d05c-44eb-b0b2-6e3760d759ef", "status": "closed", "coverage": "full", "scheduled": "2015-01-06T00:30:00+00:00", "duration": "2:15", "attendance": 16270, "lead_changes": 1, "times_tied": 2, "clock": "00:00", "quarter": 4, "home": { "name": "Celtics", "market": "Boston", "id": "583eccfa-fb46-11e1-82cb-f4ce4684ea4c", "points": 95, "scoring": [{ "number": 1, "sequence": 1, "points": 16, "type": "quarter" }, { "number": 2, "sequence": 2, "points": 20, "type": "quarter" }, { "number": 3, "sequence": 3, "points": 26, "type": "quarter" }, { "number": 4, "sequence": 4, "points": 33, "type": "quarter" }], "leaders": { "points": [{ "full_name": "Jared Sullinger", "position": "F-C", "primary_position": "PF", "jersey_number": "7", "id": "cdda9628-563e-4d3e-a660-d9665aaa94a3", "statistics": { "minutes": "33:03", "field_goals_made": 8, "field_goals_att": 16, "field_goals_pct": 50.0, "three_points_made": 0, "three_points_att": 1, "three_points_pct": 0.0, "two_points_made": 8, "two_points_att": 15, "two_points_pct": 0.533, "blocked_att": 1, "free_throws_made": 6, "free_throws_att": 6, "free_throws_pct": 100.0, "offensive_rebounds": 4, "defensive_rebounds": 4, "rebounds": 8, "assists": 1, "turnovers": 1, "steals": 2, "blocks": 4, "assists_turnover_ratio": 1.0, "personal_fouls": 5, "tech_fouls": 0, "flagrant_fouls": 0, "pls_min": -13, "points": 22 } }], "rebounds": [{ "full_name": "Jared Sullinger", "position": "F-C", "primary_position": "PF", "jersey_number": "7", "id": "cdda9628-563e-4d3e-a660-d9665aaa94a3", "statistics": { "minutes": "33:03", "field_goals_made": 8, "field_goals_att": 16, "field_goals_pct": 50.0, "three_points_made": 0, "three_points_att": 1, "three_points_pct": 0.0, "two_points_made": 8, "two_points_att": 15, "two_points_pct": 0.533, "blocked_att": 1, "free_throws_made": 6, "free_throws_att": 6, "free_throws_pct": 100.0, "offensive_rebounds": 4, "defensive_rebounds": 4, "rebounds": 8, "assists": 1, "turnovers": 1, "steals": 2, "blocks": 4, "assists_turnover_ratio": 1.0, "personal_fouls": 5, "tech_fouls": 0, "flagrant_fouls": 0, "pls_min": -13, "points": 22 } }], "assists": [{ "full_name": "Brandon Bass", "position": "F", "primary_position": "PF", "jersey_number": "30", "id": "d214d3a9-9aaa-4296-bcf0-60a1769d7a70", "statistics": { "minutes": "20:36", "field_goals_made": 3, "field_goals_att": 7, "field_goals_pct": 42.9, "three_points_made": 0, "three_points_att": 0, "three_points_pct": 0.0, "two_points_made": 3, "two_points_att": 7, "two_points_pct": 0.429, "blocked_att": 2, "free_throws_made": 2, "free_throws_att": 4, "free_throws_pct": 50.0, "offensive_rebounds": 1, "defensive_rebounds": 1, "rebounds": 2, "assists": 4, "turnovers": 1, "steals": 0, "blocks": 2, "assists_turnover_ratio": 4.0, "personal_fouls": 3, "tech_fouls": 0, "flagrant_fouls": 0, "pls_min": -4, "points": 8 } }] } }, "away": { "name": "Hornets", "market": "Charlotte", "id": "583ec97e-fb46-11e1-82cb-f4ce4684ea4c", "points": 104, "scoring": [{ "number": 1, "sequence": 1, "points": 24, "type": "quarter" }, { "number": 2, "sequence": 2, "points": 26, "type": "quarter" }, { "number": 3, "sequence": 3, "points": 29, "type": "quarter" }, { "number": 4, "sequence": 4, "points": 25, "type": "quarter" }], "leaders": { "points": [{ "full_name": "Kemba Walker", "position": "G", "primary_position": "PG", "jersey_number": "15", "id": "a35ee8ed-f1db-4f7e-bb17-f823e8ee0b38", "statistics": { "minutes": "38:53", "field_goals_made": 12, "field_goals_att": 26, "field_goals_pct": 46.2, "three_points_made": 2, "three_points_att": 7, "three_points_pct": 28.6, "two_points_made": 10, "two_points_att": 19, "two_points_pct": 0.526, "blocked_att": 3, "free_throws_made": 7, "free_throws_att": 7, "free_throws_pct": 100.0, "offensive_rebounds": 1, "defensive_rebounds": 4, "rebounds": 5, "assists": 5, "turnovers": 1, "steals": 0, "blocks": 1, "assists_turnover_ratio": 5.0, "personal_fouls": 2, "tech_fouls": 0, "flagrant_fouls": 0, "pls_min": 12, "points": 33 } }], "rebounds": [{ "full_name": "Bismack Biyombo", "position": "C-F", "primary_position": "C", "jersey_number": "8", "id": "8e3cbaa3-e30a-4cf8-aa7a-1b57f15f0f98", "statistics": { "minutes": "27:03", "field_goals_made": 2, "field_goals_att": 5, "field_goals_pct": 40.0, "three_points_made": 0, "three_points_att": 0, "three_points_pct": 0.0, "two_points_made": 2, "two_points_att": 5, "two_points_pct": 0.4, "blocked_att": 1, "free_throws_made": 0, "free_throws_att": 0, "free_throws_pct": 0.0, "offensive_rebounds": 3, "defensive_rebounds": 7, "rebounds": 10, "assists": 0, "turnovers": 1, "steals": 1, "blocks": 2, "assists_turnover_ratio": 0.0, "personal_fouls": 2, "tech_fouls": 0, "flagrant_fouls": 0, "pls_min": 8, "points": 4 } }], "assists": [{ "full_name": "Gerald Henderson", "position": "G", "primary_position": "SG", "jersey_number": "9", "id": "390c3ce6-7e5b-4aba-bd54-4dfb6add4767", "statistics": { "minutes": "33:41", "field_goals_made": 4, "field_goals_att": 9, "field_goals_pct": 44.4, "three_points_made": 0, "three_points_att": 1, "three_points_pct": 0.0, "two_points_made": 4, "two_points_att": 8, "two_points_pct": 0.5, "blocked_att": 0, "free_throws_made": 5, "free_throws_att": 6, "free_throws_pct": 83.3, "offensive_rebounds": 1, "defensive_rebounds": 2, "rebounds": 3, "assists": 8, "turnovers": 2, "steals": 1, "blocks": 2, "assists_turnover_ratio": 4.0, "personal_fouls": 2, "tech_fouls": 0, "flagrant_fouls": 0, "pls_min": 11, "points": 13 } }] } } };


}]);

angular.module('beatYourFriends.services', []);


