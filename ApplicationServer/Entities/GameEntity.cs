﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace ApplicationServer.Entities
{
    public class GameEntity : TableEntity
    {

        public GameEntity(string branch, string id, DateTime scheduled, string home, string away, string status, int winner, string apiResult)
        {
            this.PartitionKey = branch;
            this.RowKey = id;
            this.Scheduled = scheduled;
            this.Home = home;
            this.Away = away;
            this.Status = status;
            this.Winner = winner;
            this.ApiResult = apiResult;
        }

        public GameEntity() { }


        public DateTime Scheduled { get; set; }
        public string Home { get; set; }
        public string Away { get; set; }
        public string Status { get; set; }
        public string ApiResult { get; set; }
        public int Winner { get; set; }

    }
}