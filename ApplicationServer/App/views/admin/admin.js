﻿'use strict';

angular.module('beatYourFriends.admin', [])


.config(['$stateProvider', function ($stateProvider) {

    $stateProvider.state("admin", {
        url: '/admin',
        template: '<ui-view></ui-view>',
        data: {
            rule: function () {
                if (localStorage.accessToken && localStorage.userName === "admin")
                    return { newState: "admin.branches" };
                else if (localStorage.accessToken && localStorage.userName !== "admin")
                    return { newState: "user" };
                else
                    return { newState: "admin.login" };
            }
        }
    });

    $stateProvider.state("admin.login", {
        url: '/login',
        templateUrl: '/app/views/user/login/login.html',
        controller: 'loginController',
        data: {
            rule: function () {
                if (localStorage.accessToken)
                    return { newState: "admin" };
            },
            title: "Admin Login"
        }
    });

}])
