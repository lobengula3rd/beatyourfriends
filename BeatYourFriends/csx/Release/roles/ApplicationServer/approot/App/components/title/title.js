﻿'use strict';

angular.module('beatYourFriends.title', [])


.controller('titleCtrl', ['$scope', '$state', '$rootScope', function ($scope, $state, $rootScope) {

    $rootScope.$on('$stateChangeSuccess', function (evt, toState) {
        if (toState.data.title)
            $scope.title = toState.data.title;
    });

}]);