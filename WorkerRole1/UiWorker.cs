using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using Microsoft.WindowsAzure.Storage.Table;
using ApplicationServer.Entities;
using ApplicationServer.Helpers;
//using ApplicationServer.Entities;

namespace UiWorker
{
    public class UiWorker : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);
        string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");

        public override void Run()
        {
            Trace.TraceInformation("WorkerRole1 is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();
            Trace.TraceInformation("WorkerRole1 has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("WorkerRole1 is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("WorkerRole1 has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {

                newBranchAction();

                //inform all form owners that the user in the queue message had bet on 
                newFormAction();

                await Task.Delay(1000);

            }
        }

        private void newBranchAction()
        {
            CloudQueue newBranchQueue = AzureStorageHelpers.GetWebApiQueue(connectionString, "newbranch");
            // Get the next message
            CloudQueueMessage retrievedMessage = newBranchQueue.GetMessage();

            if (retrievedMessage != null)
            {
                //Process the message in less than 30 seconds, and then delete the message
                char delimiterChar = '\n';
                string[] data = retrievedMessage.AsString.Split(delimiterChar);
                var branch = data[0];
                var apiUrl = data[1];
                var apiResult = data[2];
                Trace.TraceInformation("recieved queue message. new branch: " + branch + ". apiUrl: " + apiUrl);

                //Get games from api and push them to the games table
                addGamesByBranch(branch, apiUrl, apiResult);

                //Delete queue message. There is no promise the api call succeeded
                newBranchQueue.DeleteMessage(retrievedMessage);
            }
        }

        private void addGamesByBranch(string branch, string apiUrl, string apiResult)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                var encoding = ASCIIEncoding.ASCII;

                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                {
                    string responseJson = reader.ReadToEnd();
                    dynamic schedule = JsonConvert.DeserializeObject(responseJson);

                    List<GameEntity> list = new List<GameEntity>();
                    if (schedule.weeks != null)
                    {
                        foreach (var week in schedule.weeks)
                        {
                            addGames(week, branch, list, apiResult);
                        }
                    }
                    else
                        addGames(schedule, branch, list, apiResult);


                    CloudTable GamesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "games");
                    TableBatchOperation batchOperation = new TableBatchOperation();
                    for (var i = 0; i < list.Count; i++)
                    {
                        if (batchOperation.Count == 100)
                        {
                            GamesTable.ExecuteBatch(batchOperation);
                            batchOperation = new TableBatchOperation();
                        }
                        batchOperation.Insert(list[i]);
                    }
                    if (batchOperation.Count > 0)
                        GamesTable.ExecuteBatch(batchOperation);


                }
            }
            catch (Exception e)
            {
                //could not handle the api call
            }

        }

        private void addGames(dynamic schedule, string branch, List<GameEntity> list, string apiResult)
        {
            foreach (var game in schedule.games)
            {
                var home = "";
                var away = "";
                try
                {
                    home = game.home.name;
                    away = game.away.name;
                }
                catch (Exception e)
                {
                    //could not find object name inside home/away
                    home = game.home;
                    away = game.away;
                }

                var gameApiResult = apiResult.Replace("{}", game.id.ToString());

                GameEntity entity = new GameEntity(branch, game.id.ToString(), DateTime.Parse(game.scheduled.ToString()), home, away, game.status.ToString(), -1, gameApiResult);
                list.Add(entity);
            }
        }



        private void newFormAction()
        {
            CloudQueue newFormQueue = AzureStorageHelpers.GetWebApiQueue(connectionString, "newform");
            // Get the next message
            CloudQueueMessage retrievedMessage = newFormQueue.GetMessage();
            if (retrievedMessage != null)
            {
                List<String> userList = new List<String>();
                CloudTable betsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "bets");
                // get all bets of a user and from each bet get the form owner
                TableQuery<BetEntity> rangeQuery = new TableQuery<BetEntity>().Where(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, retrievedMessage.AsString),
                        TableOperators.And,
                        TableQuery.GenerateFilterCondition("FormCreatedBy", QueryComparisons.NotEqual, retrievedMessage.AsString)));                // alert each form owner in the user bets
                foreach (BetEntity bet in betsTable.ExecuteQuery(rangeQuery))
                {
                    if (userList.Contains(bet.FormCreatedBy) == false)
                        userList.Add(bet.FormCreatedBy);
                }

                var message = retrievedMessage.AsString + " has created a new form.";

                foreach (string username in userList)
                {
                    CloudTable alertTable = AzureStorageHelpers.GetWebApiTable(connectionString, "alerts");
                    AlertEntity alert = new AlertEntity(username, message, "");
                    TableOperation insertOperation = TableOperation.Insert(alert);
                    alertTable.Execute(insertOperation);
                }

                newFormQueue.DeleteMessage(retrievedMessage);

            }
        }
    }
}
