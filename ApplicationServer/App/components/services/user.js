﻿'use strict';

angular.module('beatYourFriends.services')

.service('User', ['$http', function ($http) {

    this.register = function (user, callback) {
        $http({
            method: 'POST',
            url: '/api/Account/Register',
            responseType: 'json',
            data: user
        }).then
        (function (res) {
            if (callback)
                callback(null, res);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getCredit = function (callback) {
        $http({
            method: 'GET',
            url: '/api/Account/GetCredit',
            responseType: 'json'
        }).then
        (function (res) {
            if (callback)
                callback(null, res.data);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getAlerts = function (callback) {
        $http({
            method: 'GET',
            url: '/api/Account/GetAlerts',
            responseType: 'json'
        }).then
        (function (res) {
            if (callback)
                callback(null, res.data);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };

    this.getValues = function (callback) {
        $http({
            method: 'GET',
            url: '/api/values',
            responseType: 'json'
        }).then
        (function (res) {
            if (callback)
                callback(null, res.data.value);
        },
        function (err) {
            if (callback)
                callback(err);
        });
    };


}]);