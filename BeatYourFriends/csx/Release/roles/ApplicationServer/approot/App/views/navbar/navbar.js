﻿'use strict';

angular.module('beatYourFriends.navbar', [])

.controller('navbarController', ['$scope', '$state', '$rootScope', 'Auth', 'User', function ($scope, $state, $rootScope, Auth, User) {


    $scope.loginState = "user.login";

    var getUserCredit = function () {
        User.getCredit(function (err, res) {
            if (res || res === 0)
                $scope.credit = res;
        });
    };

    var getUserAlerts = function () {
        User.getAlerts(function (err, res) {
            if (res)
                $scope.alerts = res;
        })
    };

    var calculate = function () {

        if (localStorage.subApp === "user")
            $scope.user = true;
        else
            $scope.user = false;

        if (localStorage.accessToken) {
            $scope.connected = true;
            getUserCredit();
            getUserAlerts();
        }
        else
            $scope.connected = false;

        $scope.userName = localStorage.userName;
    };

    calculate();

    $scope.calculateAlertClass = function () {
        if ($scope.alerts && $scope.alerts.length && localStorage.accessToken)
            return 'alert-red';
        else
            return '';
    };

    $rootScope.$on('loggedIn', function () {
        calculate();
    });

    $rootScope.$on('loggedOut', function () {
        calculate();
        $scope.credit = 0;
    });

    $rootScope.$on('calcCredit', function () {
        getUserCredit();
    });

    $rootScope.$on('$stateChangeSuccess', function (evt, toState) {
        calculate();
    });

    $scope.logOut = function () {
        Auth.logOut(function (err, res) {
            $state.go(localStorage.subApp);
        });
    }







}]);