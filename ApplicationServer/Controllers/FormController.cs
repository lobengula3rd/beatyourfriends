﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure;
using System.Threading.Tasks;
using ApplicationServer.Models;
using System.IO;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using ApplicationServer.Entities;
using Newtonsoft.Json;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;


namespace ApplicationServer.Helpers
{
    [Authorize]
    public class FormController : ApiController
    {

        string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");

        // GET api/form/:date
        public IEnumerable<FormEntity> Get(DateTime creationDate)
        {
            var partitionKey = creationDate.Year + "_" + creationDate.Month + "_" + creationDate.Day;
            CloudTable formsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "forms");
            TableQuery<FormEntity> rangeQuery = new TableQuery<FormEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey),
                    TableOperators.And,
                    TableQuery.GenerateFilterConditionForDate("LastDateToBet", QueryComparisons.GreaterThan, DateTime.UtcNow)));
            return formsTable.ExecuteQuery(rangeQuery);
        }

        // GET api/form/:date
        public IEnumerable<FormEntity> Get(string formRowKey)
        {
            CloudTable formsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "forms");
            TableQuery<FormEntity> query = new TableQuery<FormEntity>().Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, formRowKey));
            return formsTable.ExecuteQuery(query);
        }


        //// GET api/game/2015-04-13
        //public IEnumerable<GameEntity> Get(string branchName, DateTime fromDate, DateTime toDate)
        //{

        //}

        // POST api/form
        public async Task<IHttpActionResult> Post(newFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindByName(User.Identity.Name);
            if (user.Credit < model.Amount)
                return BadRequest("You don't have enough credit to create this form");

            var now = DateTime.UtcNow;
            var formPartitionKey = now.Year + "_" + now.Month + "_" + now.Day;
            var formRowKey = User.Identity.Name + "_" + now.Year + "_" + now.Month + "_" + now.Day + "_" + now.Hour + "_" + now.Minute + "_" + now.Second;

            var gamesJson = model.Games;
            dynamic games = JsonConvert.DeserializeObject(gamesJson);

            DateTime checkWinnerDate = games[0].Scheduled;
            DateTime lastDateToBet = games[0].Scheduled;
            foreach (var game in games)
            {
                DateTime date = game.Scheduled;
                if (date > checkWinnerDate)
                    checkWinnerDate = date;
                if (date < lastDateToBet)
                    lastDateToBet = date;
            }

            CloudTable formTable = AzureStorageHelpers.GetWebApiTable(connectionString, "forms");
            FormEntity newForm = new FormEntity(formPartitionKey, formRowKey, model.Amount, model.Games, checkWinnerDate, User.Identity.Name, lastDateToBet);
            TableOperation insertOperation = TableOperation.Insert(newForm);
            formTable.Execute(insertOperation);

            CloudTable betTable = AzureStorageHelpers.GetWebApiTable(connectionString, "bets");
            BetEntity newBet = new BetEntity(model.Amount, model.Games, User.Identity.Name, formRowKey, User.Identity.Name);
            insertOperation = TableOperation.Insert(newBet);
            betTable.Execute(insertOperation);

            user.Credit = user.Credit - model.Amount;
            manager.Update(user);

            //Pass message to queue for worker to inform all form owners that i have bet on their forms 
            CloudQueue newFormQueue = AzureStorageHelpers.GetWebApiQueue(connectionString, "newform");
            CloudQueueMessage message = new CloudQueueMessage(User.Identity.Name);
            newFormQueue.AddMessage(message);

            return Ok();
        }

        // PUT api/game/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/game/5
        public void Delete(string branchName)
        {
        }

    }

}
