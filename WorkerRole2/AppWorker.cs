using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage.Table;
using ApplicationServer.Entities;
using ApplicationServer.Helpers;
using System.Text;

namespace AppWorker
{
    public class AppWorker : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);
        string connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");


        public override void Run()
        {
            Trace.TraceInformation("WorkerRole2 is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("WorkerRole2 has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("WorkerRole2 is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("WorkerRole2 has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {

                updateGameResults();

                calculateFormWinner();

                Trace.TraceInformation("Working");
                await Task.Delay(900000);
            }
        }

        private void updateGameResults()
        {

            //Get all branches
            CloudTable branchesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "branches");
            TableQuery<BranchEntity> query = new TableQuery<BranchEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "A-Z"));
            IEnumerable<BranchEntity> branches = branchesTable.ExecuteQuery(query);

            List<GameEntity> gamesToUpdate = new List<GameEntity>();
            CloudTable gamesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "games");

            //Iterate over each branch finding games that were already played and needs to get a result
            foreach (BranchEntity branch in branches)
            {
                string fromDate = TableQuery.GenerateFilterConditionForDate("Scheduled", QueryComparisons.GreaterThanOrEqual, DateTime.UtcNow.AddDays(-1));
                string toDate = TableQuery.GenerateFilterConditionForDate("Scheduled", QueryComparisons.LessThanOrEqual, DateTime.UtcNow);
                string finalFilter = TableQuery.CombineFilters(TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, branch.RowKey), TableOperators.And, fromDate), TableOperators.And, toDate);

                TableQuery<GameEntity> rangeQuery = new TableQuery<GameEntity>().Where(finalFilter);
                foreach (GameEntity game in gamesTable.ExecuteQuery(rangeQuery))
                {
                    if (game.Winner == -1)
                        gamesToUpdate.Add(game);
                }
            }
            if (gamesToUpdate.Count > 0)
            {
                foreach (GameEntity game in gamesToUpdate)
                {
                    try
                    {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(game.ApiResult);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        var encoding = ASCIIEncoding.ASCII;

                        using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                        {
                            string responseJson = reader.ReadToEnd();
                            dynamic result = JsonConvert.DeserializeObject(responseJson);

                            if (result.status == "closed")
                            {
                                var homeScore = Convert.ToInt32(result.home.points);
                                var awayScore = Convert.ToInt32(result.away.points);
                                var winner = -1;

                                if (homeScore > awayScore)
                                    winner = 0;
                                else if (homeScore == awayScore)
                                    winner = 1;
                                else
                                    winner = 2;

                                CloudTable gamesTableToUpdate = AzureStorageHelpers.GetWebApiTable(connectionString, "games");
                                TableQuery<GameEntity> gameToUpdateQuery = new TableQuery<GameEntity>().Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, game.RowKey));
                                IEnumerable<GameEntity> list = gamesTableToUpdate.ExecuteQuery(gameToUpdateQuery);
                                GameEntity gameToUpdate = list.First();

                                gameToUpdate.Winner = winner;
                                gameToUpdate.Status = result.status;
                                TableOperation updateOperation = TableOperation.Replace(gameToUpdate);
                                gamesTableToUpdate.Execute(updateOperation);
                            }

                        }
                    }

                    catch (Exception e)
                    {
                        //could not handle the api call
                    }
                }

            }


        }

        private void calculateFormWinner()
        {
            //finished forms could be from the past 7 days (add another 1 just to be sure)
            //partition key in form table is the date on which the form was created
            for (var i = 0; i < 8; i++)
            {
                var currentDate = DateTime.UtcNow;
                currentDate = currentDate.AddDays(-i);
                var year = currentDate.Year;
                var month = currentDate.Month;
                var day = currentDate.Day;
                var partitionKey = year.ToString() + "_" + month.ToString() + "_" + day.ToString();

                CloudTable formsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "forms");
                CloudTable gamesTable = AzureStorageHelpers.GetWebApiTable(connectionString, "games");
                CloudTable betsTable = AzureStorageHelpers.GetWebApiTable(connectionString, "bets");


                string winnerDate = TableQuery.GenerateFilterConditionForDate("CheckWinnerDate", QueryComparisons.LessThan, DateTime.UtcNow);
                string winnerExist = TableQuery.GenerateFilterCondition("Winner", QueryComparisons.Equal, null);
                string finalFilter = TableQuery.CombineFilters(TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey), TableOperators.And, winnerDate), TableOperators.And, winnerExist);
                TableQuery<FormEntity> rangeQuery = new TableQuery<FormEntity>().Where(finalFilter);

                foreach (FormEntity form in formsTable.ExecuteQuery(rangeQuery))
                {
                    //first make sure all the games has been completed and has a winner field set on them
                    dynamic games = JsonConvert.DeserializeObject(form.Games);
                    var formNotReady = false;
                    List<GameEntity> gameResults = new List<GameEntity>();
                    foreach (var game in games)
                    {
                        TableOperation retrieveOperation = TableOperation.Retrieve<GameEntity>(game.PartitionKey.ToString(), game.RowKey.ToString());
                        TableResult retrievedResult = gamesTable.Execute(retrieveOperation);
                        GameEntity gameEntity = (GameEntity)retrievedResult.Result;
                        if (gameEntity.Winner == -1)
                        {
                            formNotReady = true;
                            break;
                        }
                        else
                            gameResults.Add(gameEntity);
                    }
                    if (formNotReady == false)
                    {
                        List<string> winner = new List<string>();
                        int bestResult = -1;
                        //find all bets on this form
                        //Get all branches
                        TableQuery<BetEntity> query = new TableQuery<BetEntity>().Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, form.RowKey));
                        IEnumerable<BetEntity> bets = betsTable.ExecuteQuery(query);

                        //now calculate the winner
                        foreach (BetEntity bet in bets)
                        {
                            int correct = 0;
                            dynamic gamesInBet = JsonConvert.DeserializeObject(bet.Games);
                            for (int j = 0; j < gamesInBet.Count; j++)
                            {
                                if (gamesInBet[j].yourChoice == gameResults[j].Winner)
                                    correct++;
                            }
                            if (correct > bestResult)
                            {
                                bestResult = correct;
                                winner = new List<string>();
                                winner.Add(bet.PartitionKey);
                            }
                            else if (correct == bestResult)
                                winner.Add(bet.PartitionKey);
                        }

                        var strWinner = winner[0];
                        for (int k = 1; k < winner.Count; k++ )
                        {
                            strWinner += "/" + winner[k];
                        }
                        form.Winner = strWinner;
                        TableOperation updateOperation = TableOperation.Replace(form);
                        formsTable.Execute(updateOperation);


                        //update user money
                    }
                }

            }
        }
    }
}
